%global _empty_manifest_terminate_build 0
Name:		python-ceph-deploy
Version:	2.1.0
Release:	2
Summary:	Deploy Ceph with minimal infrastructure
License:	MIT
URL:		https://github.com/ceph/ceph-deploy
Source0:	https://github.com/ceph/ceph-deploy/archive/refs/tags/v2.1.0.tar.gz
BuildArch:	noarch
Patch1:		0001-py2-to-py3.patch 


%description


%package -n python3-ceph-deploy
Summary:	Deploy Ceph with minimal infrastructure
Provides:	python-ceph-deploy
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:	python3-remoto
%description -n python3-ceph-deploy


%package help
Summary:	Development documents and examples for ceph-deploy
Provides:	python3-ceph-deploy-doc
%description help


%prep
%autosetup -p1 -n ceph-deploy-2.1.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-ceph-deploy -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Apr 13 2022 wangzengliang<wangzengliang1@huawei.com> - 2.1.0-2
- modify some code py2 to py3

* Wed Apr 13 2022 Python_Bot <Python_Bot@openeuler.org> - 2.1.0-1
- Package Spec generated
